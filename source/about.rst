.. _about:

Виктория К.
=========

Жизнь. Судьба.

Моя жизнь
-----------

* Родилась
* Училась
* Вышла замуж
* Работаю
* Жду выхода на пенсию


Опыт работы
-----------

* Работала на шахте - добывала уголь родине, 2012-2013
* Работала на молочно-консервном заводе - делала сгущенку, 2013-2014
* Работала на железобетонном заводе - заливала стойки, 2014-2017
* Работаю техническим писателем - пишу, с 2017


Образование
-----------

* ДонНТУ, 2009-2015
* Меня учила сама жизнь
